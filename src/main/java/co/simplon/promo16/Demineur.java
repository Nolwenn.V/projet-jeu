package co.simplon.promo16;

import java.util.Scanner;

public class Demineur {
    int lignes;
    int colonnes;
    int nBA;
    int nbBombe;
    int choix;
    int nbCaseVue;
    Scanner sc = new Scanner(System.in);
    char[][] grilleJeu = new char[10][10];
    char[][] grilleBombe = new char[10][10];

    /*
     * Fonction qui permet l'initialisation de la grille de jeu
     */
    public void initialisation() {
        for (lignes = 0; lignes < 10; lignes++) {
            for (colonnes = 0; colonnes < 10; colonnes++) {
                grilleJeu[lignes][colonnes] = '-';
                grilleBombe[lignes][colonnes] = '0';

            }

        }

    }

    /*
     * Fonction qui va placer les bombes aléatoirement
     */

    public void placementBombe() {
        int compteBombe;
        for (compteBombe = 0; compteBombe < nbBombe; compteBombe++) {
            lignes = (int) (Math.random() % 10);
            colonnes = (int) (Math.random() % 10);
            if (grilleBombe[lignes][colonnes] == '0') {
                grilleBombe[lignes][colonnes] = 'B';

            } else {
                nbBombe++;
            }

        }

    }

    /*
     * Fonction qui va attendre les coordonées d'une case
     * Fonction qui va regarder si il y a une bombe sur la case
     * Retournera 1 si oui, si non retournera 0
     */

    public int RegarderMine(final int _lignes, final int _colonnes) {
        if (_lignes >= 0 && _lignes < 10 && _colonnes >= 0 && _colonnes < 10 && grilleBombe[_lignes][colonnes] == 'B')
            return 1;
        return 0;
    }

    /*
     * Fonction qui va attendre les coordonées d'une case
     * Fonction qui va permetre de connaitre le nombre de bombe autour d'une case en
     * prenant ses parametre en compte
     * Retourne le nombre de bombes se trouvant autour d'une position donée
     */

    public int nbBombeAutour(final int _lignes, final int _colonnes) {
        int nbBA = 0;
        nbBA += RegarderMine(_lignes + 1, _colonnes);
        nbBA += RegarderMine(_lignes + 1, _colonnes + 1);
        nbBA += RegarderMine(_lignes, _colonnes + 1);

        nbBA += RegarderMine(_lignes - 1, _colonnes);
        nbBA += RegarderMine(_lignes - 1, _colonnes - 1);
        nbBA += RegarderMine(_lignes, _colonnes - 1);

        nbBA += RegarderMine(_lignes + 1, _colonnes - 1);
        nbBA += RegarderMine(_lignes - 1, _colonnes + 1);

        return nbBA;

    }

    /*
     * Fonction qui va attendre les coordonées d'une case
     * Fonction qui va chercher les cases autour des bombes que si il n'y a aucune
     * bombes autour d'elle
     * Fonction qui permet de dévoiler les cases voisisnes
     */

    public void devoilerCase(final int _lignes, final int _colonnes) {
        if (_lignes >= 0 && _lignes < 10 && _colonnes >= 0 && _colonnes < 10 && grilleJeu[_lignes][_colonnes] == '-') {
            int NbMA = nbBombeAutour(_lignes, _colonnes);
            grilleJeu[_lignes][_colonnes] = (char) ('0' + NbMA);
            ++nbCaseVue;

            if (NbMA == 0) {
                devoilerCase(_lignes + 1, _colonnes);
                devoilerCase(_lignes + 1, _colonnes + 1);
                devoilerCase(_lignes, _colonnes + 1);

                devoilerCase(_lignes - 1, _colonnes);
                devoilerCase(_lignes - 1, _colonnes - 1);
                devoilerCase(_lignes, _colonnes - 1);

                devoilerCase(_lignes - 1, _colonnes + 1);
                devoilerCase(_lignes + 1, _colonnes - 1);
            }
        }

    }

    /*
     * Fonction qui permet d'afficher la grille de jeu
     */

    public void afficheJeu() {
        System.out.println("\n\n");
        System.out.println("j : 1 2 3 4 5 6 7 8 9 10");
       
        for (lignes = 0; lignes < 10; lignes++) {
            if (lignes < 9)
                System.out.print("i" + lignes +" :");
            else
                System.out.print("i" + lignes + " :");
            for (colonnes = 0; colonnes < 10; colonnes++) {
                System.out.print("- " + grilleJeu[lignes][colonnes]);
            }
            System.out.println("\n");
        }

    }

    /*
     * Fonction qui renvoie le nombre de bombes autour d'une position [i][j]
     * Renvoie -1 si on est sur une bombe
     * Sinon renvoie le nombre de bombes autour de la position
     */

    public int calculNbBombeAutour() {
        nBA = 0;
        if (grilleBombe[lignes][colonnes] == 'B') {
            nBA = -1;
        } else {
            nBA += RegarderMine(lignes + 1, colonnes);
            nBA += RegarderMine(lignes + 1, colonnes + 1);
            nBA += RegarderMine(lignes, colonnes + 1);
            nBA += RegarderMine(lignes - 1, colonnes);
            nBA += RegarderMine(lignes - 1, colonnes - 1);
            nBA += RegarderMine(lignes, colonnes - 1);
            nBA += RegarderMine(lignes + 1, colonnes - 1);
            nBA += RegarderMine(lignes - 1, colonnes + 1);
        }
        return nBA;

    }

    /*
     * Fonction qui permet de poser un drapeau ou dévoiler une case
     */

    public void jouer() {
        switch (choix) {
            case 1:
                if (calculNbBombeAutour() != -1) {
                    if (nBA == 0)
                        devoilerCase(lignes, colonnes);
                    else {
                        grilleJeu[lignes][colonnes] = (char) ('0' + nBA);
                        ++nbCaseVue;
                    }
                }
                break;

            case 2:
                grilleJeu[lignes][colonnes] = 'D';
                break;
        }

    }

    /*
     * Fonction qui permet de demander au joueur de choisir une case puis se qu'il
     * veut faire
     */

    public void choisir() {

    }

}
